package com.elrsoft.lostandfound;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Domain on 26.04.2015.
 */
@RestController
public class PostController {
    private final AtomicLong counter = new AtomicLong();


    @RequestMapping("/post")
    public Post post(@RequestParam(value = "autor", defaultValue = "Galin") String autor,
                     @RequestParam(value = "text", defaultValue = "Hi!^^^") String text) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return new Post(dateFormat.format(date), text, counter.incrementAndGet(), autor);
    }

}
