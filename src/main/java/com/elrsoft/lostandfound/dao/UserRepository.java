package com.elrsoft.lostandfound.dao;

import com.elrsoft.lostandfound.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by creative_ua on 06.05.2015.
 */
@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends MongoRepository<User, String> {
    List<User> findByFirstName(@Param("firstName") String name);
}
