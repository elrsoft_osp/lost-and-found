package com.elrsoft.lostandfound.dao;

import com.elrsoft.lostandfound.domain.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 02.05.15
 * Time: 22:53
 * To change this template use File | Settings | File Templates.
 */
@RepositoryRestResource(collectionResourceRel = "item", path = "item")
public interface ItemRepository extends MongoRepository<Item, String> {
    List<Item> findByType(@Param("type") String type);
}

