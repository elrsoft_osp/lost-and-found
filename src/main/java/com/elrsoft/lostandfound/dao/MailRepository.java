package com.elrsoft.lostandfound.dao;

import com.elrsoft.lostandfound.domain.Mail;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by creative_ua on 06.05.2015.
 */
@RepositoryRestResource(collectionResourceRel = "mail", path = "mail")
public interface MailRepository extends MongoRepository<Mail, String> {
    List<Mail> findByTitle(@Param("title") String text);
}
