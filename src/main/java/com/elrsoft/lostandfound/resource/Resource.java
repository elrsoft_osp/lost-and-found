package com.elrsoft.lostandfound.resource;

import com.elrsoft.lostandfound.domain.Comment;
import com.elrsoft.lostandfound.domain.Item;
import com.elrsoft.lostandfound.domain.Mail;
import com.elrsoft.lostandfound.domain.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by creative_ua on 08.05.2015.
 */
public class Resource {

    public static List<Mail> getListMail() {
        List<Mail> mails = new ArrayList<>();
        mails.add(new Mail("Hello", "I am Android", new Date(), new User("404 not foto", "Yura", "Yan"), new User("404 not foto", "Yana", "Kos")));
        mails.add(new Mail("I listing", "Rock", new Date(), new User("404 not foto", "Yura", "Yan"), new User("404 not foto", "Vika", "Dit")));
        mails.add(new Mail("Order", "Please read this", new Date(), new User("404 not foto", "Galin", "Fil"), new User("404 not foto", "Yura", "Yan")));
        return mails;
    }

    public static List<User> getListUser() {
        List<User> users = new ArrayList<>();
        users.add(new User("404", "Galin", "Fil", "galin@gmail.com", null));
        users.add(new User("404", "Vika", "Dit", "vika@gmail.com", null));
        users.add(new User("404", "Nazar", "Or", "nazar@gmail.com", null));
        users.add(new User("404", "Yura", "Yan", "yura@gmail.com", null));
        users.add(new User("404", "Oleg", "Pas", "oleg@gmail.com", null));
        users.add(new User("404", "Misha", "Dit", "misha@gmail.com", null));
        users.add(new User("404", "Yana", "Kos", "yana@gmail.com", null));
        users.add(new User("404", "Vasyl", "Mar", "vasyl@gmail.com", null));
        return users;
    }

    public static List<Item> getListItem() {
        List<Item> items = new ArrayList<>();
        Calendar calendar = Calendar.getInstance(Locale.UK);
        calendar.set(2015, Calendar.MAY, 8);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        items.add(new Item("found",
                "phone",
                "I found the Ipone s6",
                "404",
                calendar.getTime(),
                getDateAddWeek(calendar),
                true,
                "UK",
                "IF",
                getListComment(1),
                new User("404", "Yana", "Kos")));

        calendar.set(2015, Calendar.MAY, 10);
        items.add(new Item("found",
                "keys",
                "I found keys, 6 pieces",
                "404",
                calendar.getTime(),
                getDateAddWeek(calendar),
                true,
                "Uk",
                "Lviv",
                getListComment(2),
                new User("404", "Galin", "Fil")));

        calendar.set(2015, Calendar.MAY, 10);
        items.add(new Item("lost",
                "keys",
                "I lost keys, 4 pieces",
                "404",
                calendar.getTime(),
                getDateAddWeek(calendar),
                true,
                "Uk",
                "IF",
                getListComment(0),
                new User("404", "Nazar", "Or")));

        calendar.set(2015, Calendar.MAY, 1);
        items.add(new Item("lost",
                "book",
                "I lost book",
                "404",
                calendar.getTime(),
                getDateAddWeek(calendar),
                false,
                "Uk",
                "IF",
                getListComment(3),
                new User("404", "Yura", "Yan")));

        calendar.set(2015, Calendar.MAY, 12);
        items.add(new Item("lost",
                "dog",
                "I lost dog, name - Batman",
                "404",
                calendar.getTime(),
                getDateAddWeek(calendar),
                false,
                "Uk",
                "IF",
                getListComment(0),
                new User("404", "Vasyl", "Mar")));
        return items;
    }

    private static Date getDateAddWeek(Calendar calendar) {
        calendar.add(Calendar.DAY_OF_WEEK, 7);
        return calendar.getTime();
    }

    private static List<Comment> getListComment(int key) {
        List<Comment> comments = new ArrayList<>();
        if (key == 1) {
            comments.add(new Comment(new Date(), "firs comment", "this is first comment", new User("404", "Galin", "Fil")));
            comments.add(new Comment(new Date(), "Read this book", "It is very good book", new User("404", "Galin", "Fil")));
        } else if (key == 2) {
            comments.add(new Comment(new Date(), "I found", "I found this item", new User("404", "Nazar", "Or")));
        } else if (key == 3) {
            comments.add(new Comment(new Date(), "lost", "who could lose a", new User("404", "Yura", "Yan")));
        }
        return comments;
    }

}
