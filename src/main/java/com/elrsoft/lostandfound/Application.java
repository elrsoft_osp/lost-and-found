package com.elrsoft.lostandfound;

/**
 * Created by creative_ua on 25.04.2015.
 */

import com.elrsoft.lostandfound.dao.ItemRepository;
import com.elrsoft.lostandfound.dao.MailRepository;
import com.elrsoft.lostandfound.dao.UserRepository;
import com.elrsoft.lostandfound.domain.Mail;
import com.elrsoft.lostandfound.resource.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private MailRepository mailRepository;
    @Autowired
    private UserRepository userRepository;


    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        itemRepository.deleteAll();
        mailRepository.deleteAll();
        userRepository.deleteAll();

        mailRepository.save(Resource.getListMail());
        itemRepository.save(Resource.getListItem());
        userRepository.save(Resource.getListUser());

    }
}