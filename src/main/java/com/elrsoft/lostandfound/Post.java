package com.elrsoft.lostandfound;

import java.util.Date;

/**
 * Created by Domain on 26.04.2015.
 */
public class Post {
    private long id;
    private String autor;
    private String text;
    private String dateCreate;

    public Post(String dateCreate, String text, long id, String autor) {
        this.dateCreate = dateCreate;
        this.text = text;
        this.id = id;
        this.autor = autor;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
