package com.elrsoft.lostandfound.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by creative_ua on 06.05.2015.
 */
public class Mail {

    @Id
    private String id;
    private String title;
    private String description;
    private Date dateSend;
    private User userSend;
    private User userIn;

    public Mail() {
    }

    public Mail(String title, String description, Date dateSend, User userSend, User userIn) {
        this.title = title;
        this.description = description;
        this.dateSend = dateSend;
        this.userSend = userSend;
        this.userIn = userIn;
    }

    public User getUserSend() {
        return userSend;
    }

    public void setUserSend(User userSend) {
        this.userSend = userSend;
    }

    public User getUserIn() {
        return userIn;
    }

    public void setUserIn(User userIn) {
        this.userIn = userIn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public void setDateSend(Date dateSend) {
        this.dateSend = dateSend;
    }
}
