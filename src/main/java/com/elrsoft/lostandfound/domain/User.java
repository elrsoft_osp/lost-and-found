package com.elrsoft.lostandfound.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by creative_ua on 06.05.2015.
 */
public class User {
    @Id
    private String id;
    private String photo;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthday;

    public User() {
    }

    public User(String photo, String lastName, String firstName) {
        this.photo = photo;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public User(String photo, String firstName, String lastName, String email, Date birthday) {
        this.photo = photo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
    }


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
