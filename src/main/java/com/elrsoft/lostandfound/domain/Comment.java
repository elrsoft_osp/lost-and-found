package com.elrsoft.lostandfound.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 02.05.15
 * Time: 22:46
 * To change this template use File | Settings | File Templates.
 */
public class Comment {

    @Id
    private String id;
    private Date date;
    private String title;
    private String content;
    private User user;

    public Comment() {
    }

    public Comment(Date date, String title, String content, User user) {
        this.date = date;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
