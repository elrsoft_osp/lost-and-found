package com.elrsoft.lostandfound.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 02.05.15
 * Time: 22:39
 * To change this template use File | Settings | File Templates.
 */
public class Item {
    @Id
    private String id;

    private String type;
    private String tag;
    private String description;
    private String photo;
    private Date dateCreated;
    private Date dateActual;
    private Boolean active;
    private String location;
    private String locationDescription;
    private List<Comment> comment;
    private User user;

    public Item() {
    }

    public Item(String type, String tag, String description, String photo, Date dateCreated, Date dateActual, Boolean active, String location, String locationDescription, List<Comment> comment, User user) {
        this.type = type;
        this.tag = tag;
        this.description = description;
        this.photo = photo;
        this.dateCreated = dateCreated;
        this.dateActual = dateActual;
        this.active = active;
        this.location = location;
        this.locationDescription = locationDescription;
        this.comment = comment;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateActual() {
        return dateActual;
    }

    public void setDateActual(Date dateActual) {
        this.dateActual = dateActual;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }
}

